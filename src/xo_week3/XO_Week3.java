package xo_week3;
//GoodFuction
import java.util.Scanner;

public class XO_Week3 {

    public static Scanner s = new Scanner(System.in);
    public static char Turn = 'X';

    public static void main(String[] args) {
        char[][] board = new char[3][3];
        int row = 0;
        int col = 0;
        int count = 0;
        showWelcome();
        showLine(board);
        showUser(board, row, col, count);
    }

    public static void showUser(char[][] board, int row, int col, int count) {
        boolean player = true;
        showTable(board);
        showTurn();
        do {
            inputRowCol(board);
            if (checkWin(board, row, col)) {
                showTable(board);
                System.out.println("  ***** Game stop !! Player " + Turn + " is  Wins *****");
                break;
            }
            count++;
            showTable(board);
            swicthPlayer();
            if (checkWin(board, row, col) != true && count == 9) {
                System.out.println("Player X and O is Draw");
                break;
            }
        } while (player);
    }

    public static void inputAgain(char[][] board, int row, int col) {
        do {
            showNotAvailable();
            System.out.print("Enter new input again : ");
            row = s.nextInt() - 1;
            col = s.nextInt() - 1;
        } while (board[row][col] != '-');
    }

    public static void inputRowCol(char[][] board) {
        System.out.print("Enter input Row and Col : ");
        try {
            int row = s.nextInt() - 1;
            int col = s.nextInt() - 1;
            if (row > 2 || row < 0 || col > 2 || col < 0) {
                showWrongPosition();
                inputRowCol(board);
            } else if (board[row][col] == '-') {
                board[row][col] = Turn;
            } else {
                inputAgain(board, row, col);
                board[row][col] = Turn;
            }
        } catch (Exception e) {
            showMismatch();
            s.nextLine();
            inputRowCol(board);
        }
    }

    public static void swicthPlayer() {
        if (Turn == 'X') {
            Turn = 'O';
            System.out.println("Next Turn is : " + Turn);
        } else {
            Turn = 'X';
            System.out.println("Next Turn is : " + Turn);
        }
    }

    public static void showNotAvailable() {
        System.out.println("***** Position is not available *****");
    }

    public static void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public static void showWrongPosition() {
        System.out.println("***** Please input Row and Column [1-3] *****");
    }

    public static void showLine(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    public static void showTable(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public static void showTurn() {
        System.out.println("Start Turn is : " + Turn);
    }

    public static void showMismatch() {
        System.out.println("Input Mismatch !!");
    }

    public static boolean checkWin(char[][] board, int Row, int Col) {
        if (board[0][Col] == board[1][Col] && board[0][Col] == board[2][Col]) {
            return true;
        }
        if (board[Row][0] == board[Row][1] && board[Row][0] == board[Row][2]) {
            return true;
        }
        if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[1][1] != '-') {
            return true;
        }
        if (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[1][1] != '-') {
            return true;
        }
        return false;
    }

}
