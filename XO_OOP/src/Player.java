
public class Player {
	char name;
	int win;
	int lose;
	int draw;

	public Player(char name, int win, int lose, int draw) {
		this.name = name;
		this.win = win;
		this.lose = lose;
		this.draw = draw;
	}

	public char getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getLose() {
		return lose;
	}

	public int getDraw() {
		return draw;
	}

	public void Win() {
		win++;
	}

	public void Lose() {
		lose++;
	}

	public void Draw() {
		Draw++;
	}
}
